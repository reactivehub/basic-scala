package scala_example

case class Order(customer: Customer,
                 deliveryAddress: Option[Address],
                 payment: PaymentMethod,
                 lineItems: List[LineItem]) {

  def calculatePrice(): BigDecimal = {
    val orderPrice = lineItems.map(_.calculatePrice()).sum
    orderPrice + payment.calculateFee(orderPrice)
  }

  def sumQtyBySku(): Map[String, Double] =
    lineItems.foldLeft(Map.empty[String, Double]) {
      case (acc, li) => acc.updated(li.sku, acc.getOrElse(li.sku, 0d) + li.qty)
    }
}

case class LineItem(
    sku: String, qty: Double, price: BigDecimal, giftWrap: Option[GiftWrap]) {

  def calculatePrice(): BigDecimal =
    price * qty + giftWrap.map(_.calculatePrice()).getOrElse(0)
}

case class GiftWrap(label: String, premiumColor: Option[String]) {
  def calculatePrice(): BigDecimal = premiumColor match {
    case Some(_) => GiftWrap.level2
    case None => GiftWrap.level1
  }
}

object GiftWrap {
  val level1 = BigDecimal(5)
  val level2 = BigDecimal(10)
}
