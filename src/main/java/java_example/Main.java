package java_example;

import java.math.BigDecimal;
import java.util.*;

public class Main {
    private static List<Order> createOrders() {
        Address address1 = new Address("Hlavna", "04001", "Kosice");
        Address address2 = new Address("Komenskeho", "04001", "Kosice");
        Address address3 = new Address("Obchodná", "81106", "Bratislava");

        Customer customer1 = new Customer("Fero", address1);
        Customer customer2 = new Customer("Jozo", address2);

        Order order1 = Order.newInstance(customer1, new BankTransfer("SK341100..."));
        order1.setDeliveryAddress(address3);
        order1.addLineItem(new LineItem("iPhone6s", 1.0, new BigDecimal(649),
                new GiftWrap("Pre Katku", null)));
        order1.addLineItem(new LineItem("HTC10", 1.0, new BigDecimal(699),
                new GiftWrap("Pre Zuzku", "zlata")));

        Order order2 = Order.newInstance(customer2, new CashOnDelivery());
        order2.addLineItem(new LineItem("iPhone5s", 2.0, new BigDecimal(299), null));
        order2.addLineItem(new LineItem("HTC10", 1.0, new BigDecimal(699), null));

        return Arrays.asList(order1, order2);
    }

    private static Set<String> findPremiumColors(List<Order> orders) {
        Set<String> result = new HashSet<String>();
        for (Order order : orders) {
            for (LineItem lineItem : order.getLineItems()) {
                GiftWrap giftWrap = lineItem.getGiftWrap();
                if (giftWrap != null) {
                    String premiumColor = giftWrap.getPremiumColor();
                    if (premiumColor != null) {
                        result.add(premiumColor);
                    }
                }
            }
        }
        return result;
    }

    private static Map<String, List<Order>> groupByZip(List<Order> orders) {
        Map<String, List<Order>> result = new HashMap<String, List<Order>>();
        for (Order order : orders) {
            String zip = order.getEffectiveAddress().getZip();
            List<Order> perZip = result.get(zip);
            if (perZip == null) {
                perZip = new ArrayList<Order>();
                result.put(zip, perZip);
            }
            perZip.add(order);
        }
        return result;
    }

    public static void main(String[] args) {
        List<Order> orders = createOrders();
        System.out.println(Arrays.toString(findPremiumColors(orders).toArray()));
        System.out.println(Arrays.toString(groupByZip(orders).entrySet().toArray()));
    }
}
