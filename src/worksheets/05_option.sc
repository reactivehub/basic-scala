case class Address(street: Option[String], zip: String, city: String)
val addr = Address(Some("Hlavna"), "04001", "Kosice")
val someAddr = Some(addr)
val noneAddr = Option.empty[Address]

//===========================

//Some("Hello").isDefined
//None.isDefined

//===========================

//Some("Hello").get
//None.get

//===========================

//Some("Hello").getOrElse("World")
//None.getOrElse("World")

//===========================

//Some(addr).map(a => a.zip)
//Option.empty[Address].map(a => a.zip)

//===========================

//val optAddr = someAddr
//println(optAddr.map(_.zip).getOrElse("N/A"))

//===========================

//Some(addr).map(a => a.street)

//===========================

//val addr1 = Address(Some("Hlavna"), "04001", "Kosice")
//Some(addr1).flatMap(_.street)
//
//val addr2 = Address(None, "04001", "Kosice")
//Some(addr2).flatMap(_.street)
//
//Option.empty[Address].flatMap(_.street)

//===========================

//val optAddr = someAddr
//println(optAddr.flatMap(_.street).getOrElse("N/A"))

//===========================

//Some("Hello").filter(s => s == "Hello")
//Some("Hello").filter(s => s == "World")
//None.filter(s => s == "Hello")

//===========================

//Some("Hello").exists(s => s == "Hello")
//Some("Hello").exists(s => s == "World")
//None.exists(s => s == "Hello")

//===========================

//Some("Hello").forall(s => s == "Hello")
//Some("Hello").forall(s => s == "World")
//None.forall(s => s == "Hello")

//===========================

//val opt: Option[String] = Some("Hello")
//opt match {
//  case Some(v) => println(s"opt obsahuje $v")
//  case None    => println("opt je prazdny")
//}

//===========================

//val addr1 = Address(Some("Hlavna"), "04001", "Kosice")
//val addr2 = Address(None, "04001", "Kosice")
//
//Some(addr2) match {
//  case Some(a @ Address(None, zip, _)) =>
//    println(s"v opt je $a, zip = $zip")
//
//  case _ => println("v opt je nieco ine")
//}
