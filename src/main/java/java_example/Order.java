package java_example;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class Order {
    public static Order newInstance(Customer customer, PaymentMethod paymentMethod) {
        Order order = new Order();
        order.setCustomer(customer);
        order.setPaymentMethod(paymentMethod);
        return order;
    }

    private Customer customer;
    private Address deliveryAddress;
    private PaymentMethod paymentMethod;
    private List<LineItem> lineItems;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Address getEffectiveAddress() {
        return deliveryAddress != null ? deliveryAddress : getCustomer().getAddress();
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void addLineItem(LineItem lineItem) {
        if (lineItems == null) {
            lineItems = new Vector<LineItem>();
        }
        lineItems.add(lineItem);
    }

    public BigDecimal calculatePrice() {
        if (lineItems == null) {
            return BigDecimal.ZERO;
        }

        BigDecimal result = BigDecimal.ZERO;
        for (int i = 0; i < lineItems.size(); i++) {
            LineItem lineItem = lineItems.get(i);
            result = result.add(lineItem.calculatePrice());
        }

        return result.add(paymentMethod.calculateFee(result));
    }

    public Map<String, Double> sumQtyBySku() {
        Map<String, Double> result = new HashMap<String, Double>();

        if (lineItems == null) {
            return result;
        }

        for (LineItem lineItem : lineItems) {
            Double temp = 0d;
            if (result.containsKey(lineItem.getSku())) {
                temp = result.get(lineItem.getSku());
            }

            temp += lineItem.getQty();
            result.put(lineItem.getSku(), temp);
        }

        return result;
    }
}
