//true.toString
//1.toDouble
//1.<(10)

//===========================

//class Square
//val s = new Square

//===========================

//class Square {
//  def width: Double = ???
//  def height: Double = ???
//}
//
//val s = new Square
//println(s.width)

//===========================

//class Square(a: Double) {
//  def width: Double = a
//  def height: Double = width
//}
//
//val s = new Square(10)
//println(s.width)

//===========================

//class Square(a: Double) {
//  val width: Double = a
//  val height: Double = width
//}
//
//val s = new Square(10)
//println(s.width)

//===========================

//class Square(val width: Double) {
//  val height: Double = width
//}
//
//val s = new Square(10)
//println(s.width)

//===========================

//class Square(var width: Double) {
//  val height: Double = width
//}
//
//val s = new Square(10)
//s.width = 20

//===========================

//abstract class Shape {
//  def width: Double
//  def height: Double
//}
//
//class Square(val width: Double)
//  extends Shape {
//  val height: Double = width
//}
//
//class Rect(val width: Double,
//           val height: Double)
//  extends Shape
//
//new Square(10)
//new Rect(10, 20)

//===========================

//abstract class Shape {
//  def width: Double
//  def height: Double
//  def area: Double = width * height
//}
//
//class Square(val width: Double)
//  extends Shape {
//  val height: Double = width
//}
//
//class Rect(val width: Double,
//           val height: Double)
//  extends Shape
//
//println(new Square(10).area)
//println(new Rect(10, 20).area)

//===========================

//abstract class Shape {
//  def width: Double
//  def height: Double
//  def area: Double = width * height
//}
//
//class Circle(val r: Double) extends Shape {
//  val width = r * 2
//  val height = width
//  override def area: Double = math.Pi * r * r
//}
//
//println(new Circle(10).r)
//println(new Circle(10).area)

//===========================

//abstract class Shape {
//  def width: Double
//  def height: Double
//  def area: Double
//}
//
//trait Rectangular {
//  def width: Double
//  def height: Double
//  final def area: Double = width * height
//}
//
//class Square(val width: Double)
//  extends Shape with Rectangular {
//  val height: Double = width
//}
//
//println(new Square(10).area)

//===========================

//abstract class Shape {
//  def width: Double
//  def height: Double
//  def area: Double
//}
//
//trait Rectangular extends Shape {
//  final def area: Double = width * height
//}
//
//class Square(val width: Double)
//  extends Shape with Rectangular {
//  val height: Double = width
//}
//
//println(new Square(10).area)

//===========================

//abstract class Shape {
//  def width: Double
//  def height: Double
//  def area: Double
//}
//
//trait Rectangular { this: Shape =>
//  final def area: Double = width * height
//}
//
//class Square(val width: Double)
//  extends Shape with Rectangular {
//  val height: Double = width
//}
//
//println(new Square(10).area)

//===========================

//abstract class Shape {
//  def width: Double
//  def height: Double
//  def area: Double
//}
//
//object Point extends Shape {
//  val width = 0.0
//  val height = 0.0
//  val area = 0.0
//}
//
//println(Point.area)

//===========================

//abstract class Shape {
//  def width: Double
//  def height: Double
//  def area: Double
//}
//
//object Circle {
//  val pi = math.Pi
//  def apply(r: Double): Circle = new Circle(r)
//}
//
//class Circle(val r: Double) extends Shape {
//  val width = r * 2
//  val height = width
//  override def area: Double = math.Pi * r * r
//}
//
//new Circle(10)
//Circle.apply(10)
//Circle(10)

//===========================

//case class Address(street: String,
//                   zip: String,
//                   city: String)
//
//val a1 = Address("Hlavna", "04001", "Kosice")
//val a2 = Address("Hlavna", "04001", "Kosice")
//
//a1.toString
//
//a1 eq a2
//a1 == a2
//
//a1.hashCode()
//a2.hashCode()
//
//a1.copy(zip = "12345")
