package scala_example

import scala_example.PaymentMethod._

object Main {
  def main(args: Array[String]): Unit = {
    val orders = createOrders()
    println(premiumColors(orders))
    println(groupByZip(orders))
  }

  private def createOrders(): List[Order] = {
    val address1 = Address(Some("Hlavna"), "04001", "Kosice")
    val address2 = Address(Some("Komenskeho"), "04001", "Kosice")
    val address3 = Address(Some("Obchodná"), "81106", "Bratislava")

    val customer1 = Customer("Fero", address1)
    val customer2 = Customer("Jozo", address2)

    List(
      Order(customer1, Some(address3), BankTransfer("SK341100..."), List(
        LineItem("iPhone6s", 1.0, 649, Some(GiftWrap("Pre Katku", None))),
        LineItem("HTC10", 1.0, 699, Some(GiftWrap("Pre Zuzku", Some("zlata")))))),
      Order(customer2, None, CashOnDelivery, List(
        LineItem("iPhone5s", 2.0, 299, None),
        LineItem("HTC10", 1.0, 699, None))))
  }

  def premiumColors(orders: List[Order]): Set[String] =
    orders.flatMap(_.lineItems.foldLeft(Set.empty[String]) {
      case (acc, LineItem(_, _, _, Some(GiftWrap(_, Some(premium))))) => acc + premium
      case (acc, _) => acc
    }).toSet

  def groupByZip(orders: List[Order]): Map[String, List[Order]] =
    orders.groupBy(o => o.deliveryAddress.getOrElse(o.customer.address).zip)
}
