//new List()
//
//new ::[Int](1,
//  new ::[Int](2,
//    new ::[Int](3, Nil)))
//
//List[Int](1, 2, 3)
//List.empty[Int]

//===========================

//List(1, 2, 3).apply(0)
//List(1, 2, 3).apply(3)

//===========================

//val l = List(1, 2, 3)
//l :+ 4
//0 +: l

//===========================

//val l = List(1, 2, 3)
//l ++ List(4, 5)
//List(0, 0) ++: l

//===========================

//List(1, 2, 3).updated(1, 100)
//List(1, 2, 3).updated(3, 100)

//===========================

//List(1, 2, 3).map(a => 2 * a)

//===========================

//case class Address(street: String,
//                   zip: String,
//                   city: String)
//
//val addrs = List(
//  Address("Hlavna", "04001", "Kosice"),
//  Address("Komenskeho", "04001", "Kosice"),
//  Address("Obchodná", "81106", "Bratislava"))
//
//println(addrs.map(a => a.zip))

//===========================

//List(1, 2, 3).map(a => List(a, a))

//===========================

//List(1, 2, 3).flatMap(a => List(a, a))

//===========================

//case class Order(customer: String,
//                 lineItems: List[LineItem])
//
//case class LineItem(sku: String,
//                    qty: Double,
//                    price: BigDecimal)
//
//val orders = List(
//  Order("Fero", List(
//    LineItem("iPhone6s", 1.0, 649),
//    LineItem("HTC10", 1.0, 699))),
//  Order("Jozo", List(
//    LineItem("iPhone5s", 2.0, 299),
//    LineItem("HTC10", 1.0, 699))))
//
//println(orders.flatMap(_.lineItems.map(_.sku)))

//===========================

//List(1, 2, 3, 4).filter(_ % 2 == 0)

//===========================

//List(1, 2, 3, 4).forall(_ > 0)
//List(1, 2, 3, 4).forall(_ > 1)
//List.empty[Int].forall(_ > 1)

//===========================

//List(1, 2, 3, 4).exists(_ > 3)
//List(1, 2, 3, 4).exists(_ > 4)
//List.empty[Int].exists(_ > 4)

//===========================

//List(1, 2, 3, 4).find(_ > 1)
//List(1, 2, 3, 4).find(_ > 4)

//===========================

//List(1, 2, 3, 4).reduce((a, b) => a + b)
//List.empty[Int].reduce(_ + _)

//===========================

//List(1, 2, 3, 4).foldLeft(0)(_ + _)

//===========================

//val l = List(1, 2, 3)
//l.take(1) ++ List(100) ++ l.drop(1)
//l.take(1) ++ l.drop(1)

//===========================

//Map("a" -> 1, "b" -> 2, "c" -> 3)
//Set(1, 2, 3, 3)

//===========================

//Iterable(1, 2, 3).asJava
//
//val l = new java.util.ArrayList[Int]()
//l.add(1)
//l.add(2)
//l.add(3)
//
//l.asInstanceOf[java.lang.Iterable[Int]].asScala

//===========================

//import scala.collection.JavaConverters._
//import scala.collection.mutable
//
//mutable.Buffer(1, 2, 3).asJava
//
//val l = new java.util.ArrayList[Int]()
//l.add(1)
//l.add(2)
//l.add(3)
//
//l.asScala

//===========================

//import scala.collection.JavaConverters._
//import scala.collection.mutable
//
//Iterable(1, 2, 3).asJavaCollection
//Iterator(1, 2, 3).asJavaEnumeration
//mutable.Map(1 -> 2, 3 -> 4).asJavaDictionary

//===========================

//import scala.collection.JavaConverters._
//import scala.collection.mutable
//
//Seq(1, 2, 3).asJava
//mutable.Seq(1, 2, 3).asJava
//mutable.Set(1, 2, 3).asJava
//Map("a" -> 1, "b" -> 2, "c" -> 3).asJava
