package scala_example

sealed trait PaymentMethod {
  def calculateFee(orderPrice: BigDecimal): BigDecimal
}

object PaymentMethod {
  sealed trait FeeFree {
    final def calculateFee(orderPrice: BigDecimal): BigDecimal = 0
  }

  case object CashOnDelivery extends PaymentMethod {
    val threshold = BigDecimal(100)
    val level1 = BigDecimal(5.5)
    val freeDelivery = BigDecimal(0)

    def calculateFee(orderPrice: BigDecimal): BigDecimal =
      if (orderPrice < threshold) level1 else freeDelivery
  }

  case class BankTransfer(account: String) extends PaymentMethod with FeeFree

  case class CreditCard(
      name: String, number: String, year: Int, month: Int, cvv: Int)
      extends PaymentMethod
      with FeeFree
}
