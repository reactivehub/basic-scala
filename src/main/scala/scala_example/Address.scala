package scala_example

case class Address(street: Option[String], zip: String, city: String)
