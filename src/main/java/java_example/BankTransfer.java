package java_example;

public final class BankTransfer extends FeeFreePaymentMethod {
    private final String account;

    public BankTransfer(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }
}
