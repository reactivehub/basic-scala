package java_example;

import java.math.BigDecimal;

abstract class FeeFreePaymentMethod implements PaymentMethod {
    @Override
    public final BigDecimal calculateFee(BigDecimal orderPrice) {
        return BigDecimal.ZERO;
    }
}
