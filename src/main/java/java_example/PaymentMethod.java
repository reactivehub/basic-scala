package java_example;

import java.math.BigDecimal;

public interface PaymentMethod {
    BigDecimal calculateFee(BigDecimal orderPrice);
}
