package java_example;

public final class CreditCard extends FeeFreePaymentMethod {
    private final String name;
    private final String number;
    private final int year;
    private final int month;
    private final String cvv;

    public CreditCard(String name, String number, int year, int month, String cvv) {
        this.name = name;
        this.number = number;
        this.year = year;
        this.month = month;
        this.cvv = cvv;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public String getCvv() {
        return cvv;
    }
}
