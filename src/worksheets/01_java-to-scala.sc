//649
//1.0
//true
//"Hello"

//===========================

//1 + 1
//2 * 2
//1 < 2
//1 + 2 * 3
//(1 + 2) * 3

//===========================

//var i = 1
//i = 5

//===========================

//val i = 1

//===========================

//if (true) 1 else 0

//===========================

//var i = 0
//val w = while (i < 10) i = i + 1
//i

//===========================

//var i = 0
//val w = do i = i + 1 while (i < 10)
//i

//===========================

//import scala.util.control.NonFatal
//val t = try {
//  1 / 0
//} catch { case NonFatal(e) => println(e) }

//===========================

//val i = {
//  1
//  2
//  3
//}

//===========================

//val threshold = BigDecimal(5)
//val level1: BigDecimal = 10
//val freeDelivery: BigDecimal = 0
//
//def calculateFee(orderPrice: BigDecimal) =
//  if (orderPrice < threshold) level1 else freeDelivery
//
//calculateFee(1)
//calculateFee(100)

//===========================

//var i = 0
//def nextI = {
//  i = i + 1
//  i
//}
//
//def callByValue(i: Int): Unit =
//  for (_ <- 1 to 5) println(i)
//
//def callByName(i: => Int): Unit =
//  for (_ <- 1 to 5) println(i)
//
//callByValue(nextI)
//callByName(nextI)
