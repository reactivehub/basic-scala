package java_example;

import java.math.BigDecimal;

public final class CashOnDelivery implements PaymentMethod {
    private static final BigDecimal THRESHOLD = new BigDecimal(100);
    private static final BigDecimal LEVEL1 = new BigDecimal(5.5);
    private static final BigDecimal FREE_DELIVERY = BigDecimal.ZERO;

    @Override
    public BigDecimal calculateFee(BigDecimal orderPrice) {
        return orderPrice.compareTo(THRESHOLD) < 0 ? LEVEL1 : FREE_DELIVERY;
    }
}
