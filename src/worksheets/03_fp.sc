//val f = (x: Double) => x * x
//f(10.0)

//===========================

//val f: Double => Double = new Function1[Double, Double] {
//  def apply(x: Double): Double = x * x
//}
//
//f(10.0)

//===========================

//def sqr(x: Double): Double = x * x
//
//val f: Double => Double = sqr
//
//f(10.0)

//===========================

//def sum(f: Int => Int, a: Int, b: Int): Int =
//  if (a > b) 0
//  else f(a) + sum(f, a + 1, b)
//
//def twice(x: Int) = 2 * x
//sum(twice, 1, 3)
//
//def sqr(x: Int) = x * x
//sum(sqr, 1, 3)
//
//def cube(x: Int) = x * x * x
//sum(cube, 1, 3)

//===========================

//sum(x => 2 * x, 1, 3)
//sum(x => x * x, 1, 3)
//sum(x => x * x * x, 1, 3)
