package java_example;

import java.math.BigDecimal;

public final class LineItem {
    private final String sku;
    private final double qty;
    private final BigDecimal unitPrice;
    private final GiftWrap giftWrap;

    public LineItem(String sku, double qty, BigDecimal unitPrice, GiftWrap giftWrap) {
        this.sku = sku;
        this.qty = qty;
        this.unitPrice = unitPrice;
        this.giftWrap = giftWrap;
    }

    public String getSku() {
        return sku;
    }

    public double getQty() {
        return qty;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public GiftWrap getGiftWrap() {
        return giftWrap;
    }

    public BigDecimal calculatePrice() {
        BigDecimal wrapPrice = BigDecimal.ZERO;
        if (giftWrap != null) {
            wrapPrice = giftWrap.calculatePrice();
        }

        return unitPrice.multiply(new BigDecimal(qty)).add(wrapPrice);
    }
}
