package java_example;

import java.math.BigDecimal;

public final class GiftWrap {
    private static final BigDecimal LEVEL1 = new BigDecimal(5);
    private static final BigDecimal LEVEL2 = new BigDecimal(10);

    private final String label;
    private final String premiumColor;

    public GiftWrap(String label, String premiumColor) {
        this.label = label;
        this.premiumColor = premiumColor;
    }

    public String getLabel() {
        return label;
    }

    public String getPremiumColor() {
        return premiumColor;
    }

    public BigDecimal calculatePrice() {
        if (premiumColor != null) {
            return LEVEL2;
        } else {
            return LEVEL1;
        }
    }
}
